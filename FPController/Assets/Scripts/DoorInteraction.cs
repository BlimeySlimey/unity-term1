﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : InteractiveObject
{
    public bool locked = false;

    public KeyColour keyColour;

    public override void Activate()
    {
        if(locked == false)
        {
            //open door
            Animator targetAnim = GetComponent<Animator>();
            bool toggle = targetAnim.GetBool("Toggle Door");
            targetAnim.SetBool("Toggle Door", !toggle); 
        }
    }

    public void UnlockDoor (bool activate)
    {
        if(locked == true)
        {
            locked = false; 
            if (activate == true)
            {
                Activate(); 
            }
        }
    }
}
