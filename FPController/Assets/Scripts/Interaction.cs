﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interaction : MonoBehaviour
{

    public float distance = 2.5f;

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit i, distance) == true)
        {
            if (i.collider.GetComponent<InteractiveObject>() == true || i.collider.GetComponentInParent<InteractiveObject>() == true) 
            {
                UIManager.instance.crossHair.color = Color.green;
            }
            else
            {
                UIManager.instance.crossHair.color = Color.red;
            }
        }
        else
        {
            UIManager.instance.crossHair.color = Color.red;
        }

        if (Input.GetButtonDown("Use") == true)
        {
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit hit, distance) == true)
            {
                Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * distance, Color.green, 0.5f);
                InteractiveObject obj = hit.transform.GetComponent<InteractiveObject>();
                //if the rayhits an object

                if (obj == null)
                {
                    //grab interaction reference from the parent object of the raycast hit object
                    obj = hit.transform.GetComponentInParent<InteractiveObject>();
                }
                // if a reference to an interactable component has been made
                if (obj != null)
                {
                    //if the hit object can be interpreted as a door
                    if (obj is DoorInteraction door)
                    {  // if the door is locked
                        if (door.locked == true)
                        {
                            //if the keyring has the colour associated with the door
                            if (GetComponent<KeyRing>().CheckKeys(door.keyColour) == true)
                            {
                                door.UnlockDoor(true);
                            }

                        }
                        else
                        {
                            door.Activate();
                        }

                    }
                    else
                    {
                        obj.Activate();
                    }
                }
            }
        }
    }
}


