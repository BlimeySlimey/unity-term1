﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    public float damage;
    // creates a variable of damage

    private void OnTriggerEnter(Collider other)
    {
     if(other.tag == "Player")
        {
            other.GetComponent<Health>().TakeDamage(damage);
            // TakeDamage was from the Health script and it will damage whatever health it is on
        }
    }
}
