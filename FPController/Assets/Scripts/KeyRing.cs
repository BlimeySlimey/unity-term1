﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyRing : MonoBehaviour
{
    private List<KeyColour> collectedKeys = new List<KeyColour>(); 

    public bool PickUpKey(KeyColour colour)
    {
        if(collectedKeys.Contains(colour) == false)
        {
            collectedKeys.Add(colour);
            return true; 
        }
        return false;
        
    }

    public bool CheckKeys(KeyColour colour)
    {
        foreach (KeyColour c in collectedKeys)
        {
            if (c == colour)
            {
                return true;
            }
        }
        return false; 
    }
}
