﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : InteractiveObject
{

    public Transform target; 
    private Animator anim;

    private void Awake()
    {
        anim = transform.parent.GetComponent<Animator>();
       
    }

    public override void Activate()
    {
        anim.SetTrigger("Press Button");
        if(target != null)
        {
           if(target.tag == "Door")
            {
                Animator targetAnim = target.GetComponent<Animator>();
                bool toggle = targetAnim.GetBool("Toggle Door");
                targetAnim.SetBool("Toggle Door", !toggle); 
            }
        }
    }
}
