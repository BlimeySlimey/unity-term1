﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScoreItem : MonoBehaviour
{
    public int value = 5;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GameMaster.instance.AddScore(value);
            gameObject.SetActive(false); 
        }
    }
}

