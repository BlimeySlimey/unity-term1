﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class UIManager : MonoBehaviour
{
    public Image healthBar;
    public Text healthText;
    public Text scoreText;
    public Text timerText;
    public InputField input;
    public Image crossHair; 

    public static UIManager instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this; 
        }
    }

    private void Update()
    {
      if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void UpdateHealth(Health health)
    {
        healthBar.fillAmount = health.CurrentHealth / health.maxHealth;
        if (health.CurrentHealth > 0)
        {
            healthText.text = "Health: " + health.CurrentHealth;
        }
        else
        {
            healthText.text = "Dead";  // Player text says Dead and the SceneManager loads to the next scene. 
            SceneManager.LoadScene(2);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void UpdateScore(int score)
    {
        scoreText.text = "Score: " + score; 
    }

    public void UpdateTimer(float time)
    {
        timerText.text = time.ToString();
    }
}