﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Allows use of UI data types and functionality 

public class Health : MonoBehaviour
{
    public float maxHealth = 100;
    public float CurrentHealth { get; private set; } = 0;

    void Start()
    {
        CurrentHealth = maxHealth;
        UIManager.instance.UpdateHealth(this);
        //update Health ui stuff
    }

    public void TakeDamage(float damage)
    {
        CurrentHealth -= damage;
        if (CurrentHealth > maxHealth)
        {
            CurrentHealth = maxHealth; 
        }
        else if(CurrentHealth <= 0)
        {
            CurrentHealth = 0;
           
        }
        UIManager.instance.UpdateHealth(this);

        //update health bar value when damage is taken
    }    
}
