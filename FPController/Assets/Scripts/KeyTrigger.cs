﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyTrigger : MonoBehaviour
{
    
    public KeyColour colour;
    private MeshRenderer rend;  

    private void Awake()
    {
        rend = GetComponent<MeshRenderer>(); 
      switch(colour)
      {
            case KeyColour.red:
                rend.material.color = Color.red;
                break;
            case KeyColour.blue:
              
                rend.material.color = Color.blue;
                break;
            case KeyColour.green:
            rend.material.color = Color.green;
                break; 
            case KeyColour.yellow:
            rend.material.color = Color.yellow;
                break; 
      }   
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            KeyRing keyRing = other.GetComponent<KeyRing>(); 
            if(keyRing.PickUpKey(colour) == true)
            {
                gameObject.SetActive(false);
            }
        }
    }
}


public enum KeyColour {red, blue, green}
