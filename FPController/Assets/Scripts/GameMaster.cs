﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public static GameMaster instance;
    private int currentScore = 0; 

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        UIManager.instance.UpdateScore(currentScore); 
    }

    public void AddScore(int score)
    {
        currentScore += score;
        UIManager.instance.UpdateScore(currentScore); 
    }
}

