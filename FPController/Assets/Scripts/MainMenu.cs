﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.SceneManagement; 

public class MainMenu : MonoBehaviour
{
    public TextFileManager fileManager;
    public GameObject scoreWindow;
    public Text scoreText;

    private void Awake()
    {
        if (scoreWindow != null && scoreWindow.activeSelf == true)
        {
            ToggleScoreWindow(); 
        }
    }

    public void PlayGame ()
    {
        Debug.Log("Play le game");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); 
    }

    public void GoBack()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1); 
    }

    public void QuitGame()
    {
        Debug.Log("Quit!");
        Application.Quit(); 
    }

   private void LoadScores()
    {
        fileManager.ReadFileContents(fileManager.logName);
        scoreText.text = null; 
        for (int i = 0; i < 3; i++)
        {
            string[] s = fileManager.GetPairByKey("HighScore" + i.ToString());
            if(s.Length > 0)
            {
                scoreText.text += '\n' + fileManager.GetPairByKey(s[1])[0].ToUpper() + " " + fileManager.GetPairByKey(s[1])[1]; 
            }
        }
    }
    public void ToggleScoreWindow()
    {
        LoadScores();
        scoreWindow.SetActive(!scoreWindow.activeSelf); 
    }

    public void ClearFile()
    {
        fileManager.ClearFile(fileManager.logName);
    }
}
