﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointPlatform : MonoBehaviour
{
    public Transform[] points;
    public float speed = 2; 

    private Transform currentTarget;
    private int index = 0;

    // Start is called before the first frame update
    void Start()
    {
        if(points.Length > 0)
        {
            currentTarget = points[index];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(currentTarget !=  null)
        {
             if(Vector3.Distance(transform.position, currentTarget.position) < 0.5f)
            {
                index++;
                if(index >= points.Length)
                {
                    index = 0;
                }
                currentTarget = points[index];
            }
             else
            {
                Vector3 dir = currentTarget.position - transform.position;
                transform.position += dir * speed * Time.deltaTime; 
            }

        }
    }
}
