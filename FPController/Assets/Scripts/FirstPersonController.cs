﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    public float speed = 2.0f;
    public float sprintSpeed = 3.0f; 
    public float gravity = 3.5f;
    public float jumpforce = 0.5f;
    private float currentSpeed = 0;
    private float velocity = 0;
    private CharacterController controller;
    private Vector3 motion;

    private void Awake()
    {
        controller = GetComponent<CharacterController>(); 
    }

    // Start is called before the first frame update
    void Start()
    {
        currentSpeed = speed; 
    }

    // Update is called once per frame
    void Update()
    {
        motion = Vector3.zero; 
        if(controller.isGrounded == true)
        {
            velocity = -gravity * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space) == true)
            {
                velocity = jumpforce; 
            }
            else if(Input.GetKeyDown(KeyCode.LeftShift) == true)
            {
                if(currentSpeed != sprintSpeed)
                {
                    currentSpeed = sprintSpeed; 
                }
            }
            else if(Input.GetKeyUp(KeyCode.LeftShift) == true)
            {
                if (currentSpeed != speed)
                {
                    currentSpeed = speed;
                }
            }
        } 
        else
        {
            velocity -= gravity * Time.deltaTime; 
        }
        ApplyMovement(); 
    }


    void ApplyMovement()
    {
        motion += transform.forward * Input.GetAxisRaw("Vertical") * currentSpeed; 
        motion += transform.right * Input.GetAxisRaw("Horizontal") * currentSpeed;
        motion.y += velocity;
        controller.Move(motion * Time.deltaTime);
    }
}
