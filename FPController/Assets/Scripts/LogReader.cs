﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO; 

public class LogReader : MonoBehaviour
{
    public TextFileManager fileManager;    

    private void Start()
    {
        fileManager.CreateFile(fileManager.logName);
        fileManager.ReadFileContents(fileManager.logName);
    }
}

[System.Serializable]

public class TextFileManager
{
    [Tooltip("Defines the anme of the text file to be read/written.")]
    public string logName;
    public string[] logContents;




    public void CreateFile(string fileName)
    {
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        if (File.Exists(dirPath) == false)
        {
            Directory.CreateDirectory(Application.dataPath + "/Resources/");
            File.WriteAllText(dirPath, fileName + "\n");
        }
    }


    public string[] ReadFileContents(string fileName)
    {
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        string[] textContents = new string[0];
        if (File.Exists(dirPath) == true)
        {
            textContents = File.ReadAllLines(dirPath);
        }
        logContents = textContents;
        return textContents;
    }

    public void AddKeyValuePair(string fileName, string key, string value)
    {
        ReadFileContents(fileName);
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        string content = key + "," + value;
        string timeStamp = System.DateTime.Now.ToString(); 

        if(File.Exists(dirPath) == true)
        {
            bool contentsFound = false; 
            for(int i = 0; i < logContents.Length; i++)
            {
                if(logContents[i].Contains(key + ",") == true)
                {
                    logContents[i] = timeStamp + " - " + content;
                    contentsFound = true;
                    break; 
                }
            }

            if(contentsFound == true)
                    {
                File.WriteAllLines(dirPath, logContents);

            }
            else
            {
                File.AppendAllText(dirPath, timeStamp + " - " + content + "\n");
            }
        }
    }

    public string LocateValueByKey(string key)
    {
        string value = "";
        ReadFileContents(logName);
        foreach(string s in logContents)
        {
            if(s.Contains(key) == true)
            {
                string[] splitString = s.Split(',');
                value = splitString[splitString.Length - 1];
                break; 
            }
        }
        return value; 
    }


    public string[] GetPairByKey(string key)
    {
        string[] r = new string[0];               // creates a new empty temporary array (capacity of 0)
        string value = "";                        // creating a new local string to store the value of the CSV
        ReadFileContents(logName);                // Read the file contents
        foreach (string s in logContents)         // For every line in the file, do the following
        {
            if (s.Contains(key + ",") == true)        // if the line of the text contains the key followed by a comma
            { 
                string[] splitString = s.Split(',');     // split the string based on the position of the comma
                    value = splitString[splitString.Length - 1];   // set the value to be the last index of our split string (everything after the comma)
                r = new string[] { key, value };                   // set our temp array to a new array, that stores the original key and associated value)
                break; 
            }
        }
        return r;  // returns the temp array variable 
    }

    public void ClearFile(string fileName)
    {
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        File.WriteAllText(dirPath, fileName + '\n');
    }



}