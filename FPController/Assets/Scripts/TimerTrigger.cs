﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.SceneManagement; 

public class TimerTrigger : MonoBehaviour
{
    public TextFileManager fileManager; 
    private float timer = -1;
    private bool counting = false;
    

    // Start is called before the first frame update
    void Start()
    {
        fileManager.CreateFile(fileManager.logName);
        fileManager.ReadFileContents(fileManager.logName); 
        timer = 0;
        counting = true;
        UIManager.instance.input.gameObject.SetActive(false); 
    }

    // Update is called once per frame
    void Update()
    {
        if(counting == true)
        {
            timer += Time.deltaTime;
            UIManager.instance.UpdateTimer(timer); 
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && counting == true)
        {
            counting = false;
            UIManager.instance.input.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true; 
        }
    }

    public void SaveTimeToFile()
    {
        for (int i = 0; i < 3; i++)
        {
            // if high score index has not been saved
            if (fileManager.LocateValueByKey("HighScore" + i.ToString()) == "")
            {
              //if alias has not already been saved
              if (fileManager.LocateValueByKey(UIManager.instance.input.text) == "")
                {
                    fileManager.AddKeyValuePair(fileManager.logName, "HighScore" + i.ToString(), UIManager.instance.input.text);
                    fileManager.AddKeyValuePair(fileManager.logName, UIManager.instance.input.text, timer.ToString());
                    break;
                }
              else
                {
                    // if alias has already been saved
                    float.TryParse(fileManager.LocateValueByKey(UIManager.instance.input.text), out float r);
                    if (r > timer)
                    {
                        fileManager.AddKeyValuePair(fileManager.logName, UIManager.instance.input.text, timer.ToString());
                        continue; 
                    }
                }
            }
           else
            {
                //if score index has been found
                string k = fileManager.LocateValueByKey("HighScore" + i.ToString());
                if (k == UIManager.instance.input.text)
                {
                    float.TryParse(fileManager.LocateValueByKey(k), out float r);
                    if (r > timer)
                    {
                        fileManager.AddKeyValuePair(fileManager.logName, k, timer.ToString()); 
                    }
                }
            }
        }
        fileManager.AddKeyValuePair(fileManager.logName, UIManager.instance.input.text, timer.ToString());
        UIManager.instance.input.gameObject.SetActive(false);
        SceneManager.LoadScene(2);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
